package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import javax.swing.table.TableModel;

public class AdministratorGraphicalUserInterface extends JFrame {
    private JTable table;
    private JTextField productNameText;
    private JTextField priceLabel;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnCreate;
    private JTextField gramsText;
    private JTextField id;
    private JTextField descriptionText1;
    private  JButton btnBack;
    private JButton btnCreateCompositeProduct;
    public AdministratorGraphicalUserInterface() {

        this.setBounds(100, 200, 700, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel labelOperations = new JLabel("Here you can modify the menus");
        labelOperations.setFont(new Font("Tahoma", Font.BOLD, 10));
        labelOperations.setBounds(41, 11, 228, 23);
        getContentPane().add(labelOperations);

        btnCreate = new JButton("Insert");
        btnCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        btnCreate.setBounds(320, 45, 89, 23);
        getContentPane().add(btnCreate);

        btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        btnDelete.setBounds(320, 87, 89, 23);
        getContentPane().add(btnDelete);

        btnEdit = new JButton("Edit");
        btnEdit.setBounds(320, 136, 89, 23);
        getContentPane().add(btnEdit);
        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                try {
                    int i = table.getSelectedRow();
                    TableModel model = table.getModel();
                    id.setText(model.getValueAt(i, 0).toString());
                    productNameText.setText(model.getValueAt(i, 1).toString());
                    priceLabel.setText(model.getValueAt(i, 3).toString());
                    gramsText.setText(model.getValueAt(i, 2).toString());
                    descriptionText1.setText(model.getValueAt(i, 4).toString());
                }catch (ArrayIndexOutOfBoundsException e){

                }
            }
        });

        getContentPane().add(table);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(10, 194, 470, 180);
        getContentPane().add(scroll);

        productNameText = new JTextField();
        productNameText.setBounds(90, 46, 120, 20);
        getContentPane().add(productNameText);
        productNameText.setColumns(10);

        priceLabel = new JTextField();
        priceLabel.setBounds(90, 77, 120, 20);
        getContentPane().add(priceLabel);
        priceLabel.setColumns(10);

        JLabel productName = new JLabel("Product Name");
        productName.setBounds(10, 45, 89, 14);
        getContentPane().add(productName);

        JLabel price = new JLabel("Price");
        price.setBounds(10, 80, 67, 14);
        getContentPane().add(price);

        id = new JTextField("0");
        id.setBounds(320, 108, 96, 20);
        getContentPane().add(id);
        id.setColumns(10);
        id.setVisible(false);

        descriptionText1 = new JTextField("0");
        descriptionText1.setBounds(320, 108, 96, 20);
        getContentPane().add(id);
        descriptionText1.setColumns(10);
        descriptionText1.setVisible(false);

        gramsText = new JTextField();
        gramsText.setBounds(90, 108, 120, 20);
        getContentPane().add(gramsText);
        gramsText.setColumns(10);

        JLabel grams = new JLabel("Grams");
        grams.setBounds(10, 111, 48, 14);
        getContentPane().add(grams);

        btnBack = new JButton("Back");
        btnBack.setBounds(500, 350, 89, 23);
        getContentPane().add(btnBack);

        btnCreateCompositeProduct = new JButton("Create CompositeProduct");
        btnCreateCompositeProduct.setBounds(500, 250, 177, 23);
        getContentPane().add(btnCreateCompositeProduct);

    }

    public void insertButtonActionListener(final ActionListener actionListener) {
        btnCreate.addActionListener(actionListener);
    }
    public void deleteButtonActionListener(final ActionListener actionListener) {
        btnDelete.addActionListener(actionListener);
    }
    public void editButtonActionListener(final ActionListener actionListener) {
        btnEdit.addActionListener(actionListener);
    }
    public void backButtonActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }
    public void compositeProductButtonActionListener(final ActionListener actionListener) {
        btnCreateCompositeProduct.addActionListener(actionListener);
    }


    public String getProductNameText() {
        return productNameText.getText();
    }

    public int getPriceLabel() {
        return Integer.parseInt(priceLabel.getText());
    }

    public String getDescripionText() {
        return descriptionText1.getText();
    }

    public int getId() {
        return Integer.parseInt(id.getText());
    }

    public int getGramsLabel() {
        return Integer.parseInt(gramsText.getText());
    }

    public void setId(int id) {
        this.id.setText(Integer.toString(id));
    }

    public void refresh(HashSet<MenuItem> objects){
        TableModel model = createTable(objects).getModel();
        this.table.setModel(model);
    }

    public JTable createTable(HashSet<MenuItem> objects) {
        Object[] fieldsName ={"ID","ProductName", "Grams" ,"Price","Description"};
        Object[][] rowData = new Object[objects.size()][5];
        int i=0;
        for (MenuItem field : objects) {
            try {
                rowData[i][0] = field.getIdItem();
                rowData[i][1] = field.getProductName();
                rowData[i][2] = field.getGrams();
                rowData[i][3] = field.getPrice();
                rowData[i][4] = field.getDescription();
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        return new JTable(rowData, fieldsName);
    }
}
