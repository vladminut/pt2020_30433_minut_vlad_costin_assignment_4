package presentation;

import business.MenuItem;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

public class WaiterGraphicalUserInterface extends JFrame {

    private JTextField productName;
    private JTextField quantity;
    private JTextField price;
    private JTextField id;
    private JTextField description;
    private JTable table;
    private JButton generateBillButton;
    private JButton btnSeeOrder;
    private JButton btnAdd;
    private JButton btnBack;
    private JButton createOrder;
    private JTextField tableText;
    private JTextField dateText;
    private JLabel lblProductName;
    private JLabel lblQuantity;
    private JLabel lblPrice ;
    private JLabel lblTable;
    private JLabel lblDate;
    private JButton btnViewAllOrders;

    public WaiterGraphicalUserInterface() {
        this.setBounds(100, 100, 486, 488);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        productName = new JTextField();
        productName.setBounds(45, 51, 96, 20);
        getContentPane().add(productName);
        productName.setColumns(10);

        quantity = new JTextField();
        quantity.setBounds(171, 51, 96, 20);
        getContentPane().add(quantity);
        quantity.setColumns(10);

        id = new JTextField();
        id.setBounds(171, 51, 96, 20);
        getContentPane().add(id);
        id.setColumns(10);
        id.setVisible(false);
        description = new JTextField();
        description.setBounds(171, 51, 96, 20);
        getContentPane().add(id);
        description.setColumns(10);
        description.setVisible(false);

        price = new JTextField();
        price.setBounds(306, 51, 96, 20);
        getContentPane().add(price);
        price.setColumns(10);

        tableText = new JTextField();
        tableText.setBounds(37, 359, 96, 20);
        getContentPane().add(tableText);
        tableText.setColumns(10);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        dateText = new JTextField(dateFormat.format(date).toString());
        dateText.setBounds(171, 359, 96, 20);
        getContentPane().add(dateText);
        dateText.setColumns(10);

        lblTable = new JLabel("Table");
        lblTable.setBounds(37, 334, 48, 14);
        getContentPane().add(lblTable);

        lblDate = new JLabel("Date");
        lblDate.setBounds(171, 334, 48, 14);
        getContentPane().add(lblDate);


        table=new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                try{
                    int i = table.getSelectedRow();
                    TableModel model = table.getModel();
                    id.setText(String.valueOf(Integer.parseInt(model.getValueAt(i, 0).toString())));
                    productName.setText(model.getValueAt(i, 1).toString());
                    price.setText(model.getValueAt(i, 3).toString());
                    description.setText(model.getValueAt(i, 4).toString());
                }catch (ArrayIndexOutOfBoundsException e){

                }

            }
        });
        getContentPane().add(table);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(10, 138, 435, 185);
        getContentPane().add(scroll);

        btnAdd = new JButton("ADD");
        btnAdd.setBounds(306, 104, 96, 23);
        getContentPane().add(btnAdd);

        createOrder = new JButton("Create Order");
        createOrder.setBounds(306, 358, 117, 23);
        getContentPane().add(createOrder);

        lblProductName = new JLabel("Product Name");
        lblProductName.setBounds(45, 26, 78, 14);
        getContentPane().add(lblProductName);

        lblQuantity = new JLabel("Quantity");
        lblQuantity.setBounds(171, 26, 48, 14);
        getContentPane().add(lblQuantity);

        lblPrice = new JLabel("Price\\buc");
        lblPrice.setBounds(306, 26, 48, 14);
        getContentPane().add(lblPrice);

        btnSeeOrder = new JButton("Compute Price");
        btnSeeOrder.setBounds(179, 415, 117, 23);
        getContentPane().add(btnSeeOrder);

        generateBillButton = new JButton("Generate Bill");
        generateBillButton.setBounds(35, 415, 106, 23);
        getContentPane().add(generateBillButton);

        btnBack = new JButton("Back");
        btnBack.setBounds(381, 11, 64, 23);
        getContentPane().add(btnBack);


        btnViewAllOrders = new JButton("View all Orders");
        btnViewAllOrders.setBounds(306, 415, 137, 23);
        getContentPane().add(btnViewAllOrders);
    }

    public void createOrderTButtonTActionListener(final ActionListener actionListener) {
        createOrder.addActionListener(actionListener);
    }

    public void addTButtonTActionListener(final ActionListener actionListener) {
        btnAdd.addActionListener(actionListener);
    }

    public void viewOrdersButtonTActionListener(final ActionListener actionListener) {
        btnViewAllOrders.addActionListener(actionListener);
    }


    public void seeOrderTButtonActionListener(final ActionListener actionListener) {
        btnSeeOrder.addActionListener(actionListener);
    }

    public void generateBillButtonActionListener(final ActionListener actionListener) {
        generateBillButton.addActionListener(actionListener);
    }
    public void backTButtonTActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }

    public String getProductName() {
        return productName.getText();
    }

    public int getQuantity() {
        return Integer.parseInt(quantity.getText());
    }

    public int getPrice() {
        return Integer.parseInt(price.getText());
    }

    public int getId() {
        return Integer.parseInt(id.getText());
    }

    public int getTableText() {
        return Integer.parseInt(tableText.getText());
    }

    public String getDateText() {
        return dateText.getText();
    }

    public String getDescription(){
        return description.getText();
    }
    public void refresh(HashSet<MenuItem> objects){
        TableModel model = createTable(objects).getModel();
        this.table.setModel(model);
    }

    public JTable createTable(HashSet<MenuItem> objects) {
        Object[] fieldsName ={"ID","ProductName", "Grams" ,"Price","Description"};
        Object[][] rowData = new Object[objects.size()][5];
        int i=0;
        for (MenuItem field : objects) {
            try {
                rowData[i][0] = field.getIdItem();
                rowData[i][1] = field.getProductName();
                rowData[i][2] = field.getGrams();
                rowData[i][3] = field.getPrice();
                rowData[i][4] = field.getDescription();
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        return new JTable(rowData, fieldsName);
    }
    public void visibilityCreate(){

        btnSeeOrder.setVisible(false);
        generateBillButton.setVisible(false);
        createOrder.setVisible(true);
        tableText.setVisible(true);
        lblTable.setVisible(true);
        dateText.setVisible(true);
        lblDate.setVisible(true);
    }
    public void visibilityAddToOrder(){
        btnSeeOrder.setVisible(true);
        generateBillButton.setVisible(true);
        createOrder.setVisible(false);
        tableText.setVisible(false);
        lblTable.setVisible(false);
        dateText.setVisible(false);
        lblDate.setVisible(false);

    }
}
