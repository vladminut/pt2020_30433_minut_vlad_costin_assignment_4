package presentation;

import business.Order;
import business.Restaurant;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ViewAllOrders extends JFrame {

    private JButton btnBack;
    private JTable table;
    private JTextField id;
    private JTextField date;
    private JTextField tableText;
    private JLabel lblRon;
    private JButton viewOrder;

    public ViewAllOrders() {
        this.setBounds(100, 100, 457, 373);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        btnBack = new JButton("Back");
        btnBack.setBounds(318, 11, 89, 23);
        getContentPane().add(btnBack);


        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                try{
                    int i = table.getSelectedRow();
                    TableModel model = table.getModel();
                    id.setText(String.valueOf(Integer.parseInt(model.getValueAt(i, 0).toString())));
                    date.setText(model.getValueAt(i, 1).toString());
                    tableText.setText(model.getValueAt(i, 2).toString());
                }catch (ArrayIndexOutOfBoundsException e){

                }

            }
        });
        getContentPane().add(table);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(39, 70, 366, 203);
        getContentPane().add(scroll);

        id = new JTextField();
        id.setBounds(311, 67, 106, 20);
        getContentPane().add(id);
        id.setColumns(10);
        id.setVisible(false);

        date = new JTextField();
        date.setBounds(311, 109, 106, 20);
        getContentPane().add(date);
        date.setColumns(10);
        date.setVisible(false);

        tableText = new JTextField();
        tableText.setBounds(311, 150, 106, 20);
        getContentPane().add(tableText);
        tableText.setColumns(10);
        tableText.setVisible(false);

        lblRon = new JLabel("0 RON");
        lblRon.setBounds(28, 304, 48, 14);
        getContentPane().add(lblRon);

        viewOrder = new JButton("View Order");
        viewOrder.setBounds(169, 300, 106, 23);
        getContentPane().add(viewOrder);
    }

    public void backButtonActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }

    public void viewOrderButtonActionListener(final ActionListener actionListener) {
        viewOrder.addActionListener(actionListener);
    }

    public void refresh(Restaurant restaurant){
        TableModel model = createTable(restaurant).getModel();
        this.table.setModel(model);
    }

    public JTable createTable(Restaurant restaurant) {
        Object[] fieldsName ={"ID","Date", "Table"};
        Object[][] rowData = new Object[restaurant.getOrders().size()][3];
        int i=0;
        for (Order field : restaurant.getOrders().keySet()) {
            try {
                rowData[i][0] = field.getOrderID();
                rowData[i][1] = field.getDate();
                rowData[i][2] = field.getTable();
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        return new JTable(rowData, fieldsName);
    }

    public int getId() {
        return Integer.parseInt(id.getText());
    }

    public String getDate() {
        return date.getText();
    }

    public int getTableText() {
        return Integer.parseInt(tableText.getText());
    }

}
