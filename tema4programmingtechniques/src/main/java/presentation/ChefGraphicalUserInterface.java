package presentation;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;

public class ChefGraphicalUserInterface extends JFrame implements Observer {

    private JButton btnBack;
    private JLabel status;

    public ChefGraphicalUserInterface() {
        this.setBounds(100, 100, 400, 152);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        status = new JLabel("Waiting...");
        status.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 18));
        status.setBounds(30, 24, 314, 76);
        getContentPane().add(status);

        btnBack = new JButton("Back");
        btnBack.setBounds(290, 29, 89, 23);
        getContentPane().add(btnBack);
    }



    public void backButtonActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }

    public void update(Observable o, Object arg) {
        status.setText("I've been notified and I started to cook");

    }
}
