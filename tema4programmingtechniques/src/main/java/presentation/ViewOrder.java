package presentation;

import javax.swing.*;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ViewOrder extends JFrame {
    private JList list;
    private JButton btnBack;

    public ViewOrder() {
        this.setBounds(100, 100, 469, 314);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel lblYourOrderIs = new JLabel("Your order is:");
        lblYourOrderIs.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 22));
        lblYourOrderIs.setBounds(30, 24, 214, 26);
        getContentPane().add(lblYourOrderIs);

        list = new JList();
        list.setBounds(30, 76, 402, 167);
        getContentPane().add(list);

        btnBack = new JButton("Back");
        btnBack.setBounds(343, 29, 89, 23);
        getContentPane().add(btnBack);
    }

    DefaultListModel<String> model = new DefaultListModel();
    public void addToList(ArrayList<String> s) {
        model.clear();
        for(String s1 : s) {
            model.addElement(s1);
        }
        list.setModel(model);
    }

    public void backButtonActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }
}
