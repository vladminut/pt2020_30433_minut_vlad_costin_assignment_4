package presentation;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;


public class LoginView extends JFrame {
    private JButton btnAdministrator;
    private JButton btnWaiter;
    private JButton btnChef;

    public LoginView() {
        this.setBounds(100, 100, 366, 333);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel lblWelcome = new JLabel("Hello there, please choose!");
        lblWelcome.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));
        lblWelcome.setBounds(23, 12, 300, 37);
        getContentPane().add(lblWelcome);


        getContentPane().add(lblWelcome);
        btnAdministrator = new JButton("Administrator");
        btnAdministrator.setBounds(112, 86, 112, 37);
        getContentPane().add(btnAdministrator);

        btnWaiter = new JButton("Waiter");
        btnWaiter.setBounds(112, 147, 112, 37);
        getContentPane().add(btnWaiter);

        btnChef = new JButton("Chef");
        btnChef.setBounds(112, 214, 112, 40);
        getContentPane().add(btnChef);


    }
    public void adminButtonActionListener(final ActionListener actionListener) {
        btnAdministrator.addActionListener(actionListener);
    }
    public void waiterButtonActionListener(final ActionListener actionListener) {
        btnWaiter.addActionListener(actionListener);
    }
    public void chefButtonActionListener(final ActionListener actionListener) {
        btnChef.addActionListener(actionListener);
    }
}
