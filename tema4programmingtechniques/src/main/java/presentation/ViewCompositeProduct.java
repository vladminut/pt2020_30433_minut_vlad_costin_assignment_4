package presentation;
import business.MenuItem;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;

public class ViewCompositeProduct extends JFrame {
    private JTable table;
    private JTextField name;
    private JTextField gramsText;
    private JTextField id;
    private JTextField descriptionText1;
    private JTextField productNameText;
    private JTextField price;
    private JButton btnAdd;
    private JButton btnCreateproduct;
    private JButton btnBack;


    public ViewCompositeProduct() {
        this.setBounds(100, 100, 485, 294);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                try {
                    int i = table.getSelectedRow();
                    TableModel model = table.getModel();
                    id.setText(model.getValueAt(i, 0).toString());
                    productNameText.setText(model.getValueAt(i, 1).toString());
                    price.setText(model.getValueAt(i, 3).toString());
                    gramsText.setText(model.getValueAt(i, 2).toString());
                }catch (ArrayIndexOutOfBoundsException e){

                }
            }
        });
        getContentPane().add(table);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(25, 34, 235, 185);
        getContentPane().add(scroll);

        productNameText = new JTextField();
        productNameText.setBounds(320, 46, 120, 20);
        getContentPane().add(productNameText);
        productNameText.setColumns(10);
        productNameText.setVisible(false);

        price = new JTextField();
        price.setBounds(320, 77, 120, 20);
        getContentPane().add(price);
        price.setColumns(10);
        price.setVisible(false);

        id = new JTextField("0");
        id.setBounds(320, 108, 96, 20);
        getContentPane().add(id);
        id.setColumns(10);
        id.setVisible(false);


        gramsText = new JTextField();
        gramsText.setBounds(320, 137, 120, 20);
        getContentPane().add(gramsText);
        gramsText.setColumns(10);
        gramsText.setVisible(false);

        btnAdd = new JButton("Add");
        btnAdd.setBounds(370, 172, 89, 23);
        getContentPane().add(btnAdd);

        btnCreateproduct = new JButton("Create");
        btnCreateproduct.setBounds(370, 196, 89, 23);
        getContentPane().add(btnCreateproduct);

        name = new JTextField();
        name.setBounds(363, 66, 96, 20);
        getContentPane().add(name);
        name.setColumns(10);

        btnBack = new JButton("Back");
        btnBack.setBounds(370, 11, 89, 23);
        getContentPane().add(btnBack);

        JLabel lblProductName = new JLabel("Product Name");
        lblProductName.setBounds(270, 69, 81, 14);
        getContentPane().add(lblProductName);

    }

    public void backButtonActionListener(final ActionListener actionListener) {
        btnBack.addActionListener(actionListener);
    }
    public void addButtonActionListener(final ActionListener actionListener) {
        btnAdd.addActionListener(actionListener);
    }
    public void createButtonActionListener(final ActionListener actionListener) {
        btnCreateproduct.addActionListener(actionListener);
    }
    public String getProductNameText() {
        return productNameText.getText();
    }
    public String getProductName() {
        return name.getText();
    }

    public int getPriceLabel() {
        return Integer.parseInt(price.getText());
    }

    public int getId() {
        return Integer.parseInt(id.getText());
    }

    public int getGramsLabel() {
        return Integer.parseInt(gramsText.getText());
    }
    public void setId(int id) {
        this.id.setText(Integer.toString(id));
    }

    public void refresh(HashSet<MenuItem> objects){
        TableModel model = createTable(objects).getModel();
        this.table.setModel(model);
    }

    public JTable createTable(HashSet<MenuItem> objects) {
        Object[] fieldsName ={"ID","ProductName", "Grams" ,"Price"};
        Object[][] rowData = new Object[objects.size()][4];
        int i=0;
        for (MenuItem field : objects) {
            try {
                rowData[i][0] = field.getIdItem();
                rowData[i][1] = field.getProductName();
                rowData[i][2] = field.getGrams();
                rowData[i][3] = field.getPrice();
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        return new JTable(rowData, fieldsName);
    }

    public void refreshText(){
        name.setText("");
    }

}
