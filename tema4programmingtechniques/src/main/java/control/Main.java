package control;

//COMANDA DE RULARE: java -cp PT2020_30433_Minut_Vlad_Costin_Assignment_4.jar control.Main
public  class Main {

    public static void main(String[] args) {
        Controller controller = new Controller();
        try {
            controller.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
