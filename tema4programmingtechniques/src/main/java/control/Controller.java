package control;
import business.*;
import data.*;
import presentation.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class Controller {
    private AdministratorGraphicalUserInterface adminView;
    private WaiterGraphicalUserInterface waiterView;
    private LoginView loginView;
    private Restaurant restaurant;
    private ViewOrder viewOrder;
    private ViewAllOrders viewAllOrders;
    private RestaurantSerializator restaurantSerializator;
    private ChefGraphicalUserInterface chefView;
    private ViewCompositeProduct viewCompositeProduct;
    Order currentOrder;
    private int price=0;

    public void start()  {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.getStackTrace();

        }
        adminView = new AdministratorGraphicalUserInterface();
        waiterView = new WaiterGraphicalUserInterface();
        loginView = new LoginView();
        viewOrder = new ViewOrder();
        viewAllOrders = new ViewAllOrders();
        chefView = new ChefGraphicalUserInterface();
        viewCompositeProduct = new ViewCompositeProduct();
        restaurantSerializator = new RestaurantSerializator();

        try {
            restaurant = restaurantSerializator.read();
            adminView.refresh(restaurant.getMenu());
            waiterView.refresh(restaurant.getMenu());
        }catch(NullPointerException e) {
            restaurant = new Restaurant();
        }

        restaurant.addObserver(chefView);


        loginView.setVisible(true);
        initializeButtonListeners();
    }

    void initializeButtonListeners() {

        loginView.adminButtonActionListener(e->{
            adminView.setVisible(true);
            loginView.setVisible(false);
        });

        loginView.waiterButtonActionListener(e->{
            waiterView.setVisible(true);
            loginView.setVisible(false);
            waiterView.refresh(restaurant.getMenu());
            waiterView.visibilityCreate();
        });

        loginView.chefButtonActionListener(e->{
            chefView.setVisible(true);
        });

        adminView.insertButtonActionListener(e -> {
            try {
                if(adminView.getPriceLabel()<=0 || adminView.getGramsLabel()<=0)
                    throw new NumberFormatException("Not a good price or grams!");
                adminView.setId(restaurant.getMaxId()+1);
                restaurant.createMenuItem(adminView.getId(),adminView.getProductNameText(), adminView.getGramsLabel(), adminView.getPriceLabel(), "BaseProduct");
                adminView.refresh(restaurant.getMenu());
                restaurantSerializator.write(restaurant);
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please complete all the fildes corectly!");
            }
        });
        adminView.deleteButtonActionListener(e -> {
            try {
                restaurant.deleteMenuItem(adminView.getId(), adminView.getProductNameText(), adminView.getGramsLabel(), adminView.getPriceLabel(), adminView.getDescripionText());
                adminView.refresh(restaurant.getMenu());
                restaurantSerializator.write(restaurant);
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please complete all the fildes corectly!");
            }
        });

        adminView.editButtonActionListener(e -> {
            try {
                if(adminView.getPriceLabel()<=0 || adminView.getGramsLabel()<=0)
                    throw new NumberFormatException("Not a good price or grams!");
                restaurant.editMenuItem(adminView.getId(),adminView.getProductNameText(), adminView.getGramsLabel(), adminView.getPriceLabel(), adminView.getDescripionText());
                adminView.refresh(restaurant.getMenu());
                restaurantSerializator.write(restaurant);
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please complete all the fildes!");
            }
        });

        adminView.backButtonActionListener(e->{

            loginView.setVisible(true);
            adminView.setVisible(false);
        });

        adminView.compositeProductButtonActionListener(e->{
            viewCompositeProduct.setVisible(true);
            viewCompositeProduct.refresh(restaurant.getMenu());
        });

        viewCompositeProduct.backButtonActionListener(e->{
            viewCompositeProduct.setVisible(false);
        });

        Set<MenuItem> composeProducts = new HashSet<>();
        viewCompositeProduct.addButtonActionListener(e->{
            try {
                composeProducts.add(new BaseProduct(viewCompositeProduct.getId(), viewCompositeProduct.getProductNameText(), viewCompositeProduct.getPriceLabel(), viewCompositeProduct.getGramsLabel()));
                System.out.println(viewCompositeProduct.getProductNameText());
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please selecte again!");
            } catch (NullPointerException e2) {
                JOptionPane.showMessageDialog(null, "Please selecte again!");
            }
        });
        viewCompositeProduct.createButtonActionListener(e->{
            try{
                CompositeProduct compositeProduct= new CompositeProduct();
                if(viewCompositeProduct.getProductName().equals(""))
                    throw new NumberFormatException("Please inserte a name!");
                compositeProduct.createProuct(composeProducts, viewCompositeProduct.getProductName());
                int price=0;
                for(MenuItem item : composeProducts){
                    price+=item.computePrice(item);
                }
                viewCompositeProduct.setId(restaurant.getMaxId()+1);
                restaurant.createMenuItem(viewCompositeProduct.getId(),viewCompositeProduct.getProductName(), price,compositeProduct.computeGrams(composeProducts), "CompositeProduct");
                viewCompositeProduct.refresh(restaurant.getMenu());
                adminView.refresh(restaurant.getMenu());
                viewCompositeProduct.refreshText();
                restaurantSerializator.write(restaurant);
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please inserte a name!");
            }
        });
        waiterView.backTButtonTActionListener(e->{
            waiterView.setVisible(false);
            loginView.setVisible(true);
        });
        ArrayList<String> s = new ArrayList<>();
        Set<MenuItem> items = new HashSet<>();


        waiterView.createOrderTButtonTActionListener(e->{
            try {

                currentOrder = new Order(restaurant.getMaxOrder() + 1, waiterView.getDateText(), waiterView.getTableText());
                if(currentOrder == null && items == null)
                    throw new NullPointerException("Order or menuItem null!");
                restaurant.createOrder(currentOrder, items);
                restaurantSerializator.write(restaurant);
                waiterView.visibilityAddToOrder();
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please complete insert a table number!");
            } catch (NullPointerException e1) {
                JOptionPane.showMessageDialog(null, "Order or menuItem null!");
            }
        });


        waiterView.addTButtonTActionListener(e->{
            try{
                MenuItem item;
                if(waiterView.getDescription().equals("CompositeProduct"))
                    item = new CompositeProduct(waiterView.getId(),waiterView.getProductName(),waiterView.getPrice()*waiterView.getQuantity());
                else
                    item = new BaseProduct(waiterView.getId(),waiterView.getProductName(),waiterView.getPrice()*waiterView.getQuantity());
                price += waiterView.getPrice()*waiterView.getQuantity();
                items.add(item);
                restaurantSerializator.write(restaurant);
                s.add("Produce Name: "+ waiterView.getProductName() + "      Price: " + Integer.toString(waiterView.getPrice()) + "       Quantity: " + Integer.toString(waiterView.getQuantity()));
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please complete all the fildes!");
            }
        });


        waiterView.seeOrderTButtonActionListener(e->{
            try{
                s.add("Total price is: " + price);
                viewOrder.addToList(s);
                viewOrder.setVisible(true);
            } catch (NullPointerException e1) {
                JOptionPane.showMessageDialog(null, "Order null!");
            }

        });

        viewAllOrders.viewOrderButtonActionListener(e->{
            try{
                Order order = new Order(viewAllOrders.getId(), viewAllOrders.getDate(), viewAllOrders.getTableText());
                ArrayList<String> itemsOrder = new ArrayList<>();
                for(MenuItem item : restaurant.getOrders().get(order)){
                    itemsOrder.add("Produce Name: "+ item.getProductName() + "      Price: " + Integer.toString(item.getPrice()));
                }
                viewOrder.addToList(itemsOrder);
                viewOrder.setVisible(true);
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Please selecte again!");
            } catch (NullPointerException e2) {
                JOptionPane.showMessageDialog(null, "Please selecte again!");
            }
        });
        viewOrder.backButtonActionListener(e->{
            viewOrder.setVisible(false);
        });

        waiterView.generateBillButtonActionListener(e->{
            try {
                if (s == null)
                    throw new NullPointerException("String null!");
                restaurant.generateBill(s);
                waiterView.visibilityCreate();
                System.out.println(restaurant);
                restaurant.setMenu(restaurant.getMenu());
                restaurant.setOrders(restaurant.getOrders());
                restaurantSerializator.write(restaurant);
                items.clear();
                s.clear();
                price=0;
            }catch(NullPointerException e1){
                JOptionPane.showMessageDialog(null, "Can not compute bill!");
            }

        });

        waiterView.viewOrdersButtonTActionListener(e->{
            viewAllOrders.setVisible(true);
            waiterView.setVisible(false);
            viewAllOrders.refresh(restaurant);
        });
        viewAllOrders.backButtonActionListener(e->{
            waiterView.setVisible(true);
            viewAllOrders.setVisible(false);

        });
        chefView.backButtonActionListener(e->{
            loginView.setVisible(true);
            chefView.setVisible(false);
        });
    }
}
