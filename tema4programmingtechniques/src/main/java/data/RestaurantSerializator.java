package data;

import business.Restaurant;

import java.io.*;

public class RestaurantSerializator {

    public void write(Restaurant restaurant) {
        FileOutputStream file= null;
        ObjectOutputStream outputStream = null;
        try {
            file = new FileOutputStream("ser.txt");
            outputStream= new ObjectOutputStream(file);
            outputStream.writeObject(restaurant);

        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public Restaurant read() {
        FileInputStream file;
        ObjectInputStream inputStream = null;
        Restaurant restaurant = new Restaurant();
        try {
            file = new FileInputStream(new File("ser.txt"));
            try {
                inputStream = new ObjectInputStream(file);
                restaurant = (Restaurant) inputStream.readObject();
            }catch(EOFException e2){
                return null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }  finally{
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return restaurant;
    }
}
