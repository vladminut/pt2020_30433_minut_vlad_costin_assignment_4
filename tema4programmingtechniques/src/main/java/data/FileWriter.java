package data;

import java.io.*;
import java.util.List;

public class FileWriter {

    public void write(List<String> s) {
        BufferedWriter outputStream = null;
        try {
            outputStream = new BufferedWriter(new java.io.FileWriter("bill.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write("This is what you ordered:");
            outputStream.newLine();
            outputStream.newLine();
            for (String product : s) {
                outputStream.write(product);
                outputStream.newLine();

            }
            outputStream.newLine();
            outputStream.write("Have a nice day!");
            outputStream.newLine();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
