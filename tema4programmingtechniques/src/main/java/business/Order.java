package  business;
import java.io.Serializable;

public class Order implements Serializable {

    private int orderID;
    private String date;
    private int table;

    public Order(int orderID, String date, int table) {
        this.orderID = orderID;
        this.date = date;
        this.table = table;
    }

    public int getOrderID() {
        return orderID;
    }
    public String getDate() {
        return date;
    }
    public int getTable() {
        return table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                table == order.table &&
                date.equals( order.date);
    }
    @Override
    public int hashCode() {
        return 31*orderID+table;
    }
}
