package business;

public class BaseProduct extends MenuItem {

    public BaseProduct(int id,String productName, int grams, int price, String description) {
        super(id,productName, grams, price, description);
    }
    public BaseProduct(int id,String productName, int grams, int price) {
        super(id,productName, grams, price);
    }
    public BaseProduct(int id, String productName, int price) {
        super(id, productName, price);
    }


    @Override
    public int computePrice(MenuItem item) {
        return getPrice();
    }

}
