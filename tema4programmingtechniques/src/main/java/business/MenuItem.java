package  business;

import java.io.Serializable;
import java.util.Objects;

public abstract class MenuItem implements Serializable {

    private int idItem;
    private String productName;
    private int grams;
    private int price;
    private String description;

    public MenuItem(int id, String productName, int grams, int price, String description) {
        this.idItem = id;
        this.productName = productName;
        this.grams = grams;
        this.price = price;
        this.description = description;
    }

    public MenuItem(int id, String productName, int grams, int price) {
        this.idItem = id;
        this.productName = productName;
        this.grams = grams;
        this.price = price;
    }
    public MenuItem(int id, String productName, int price) {
        this.idItem = id;
        this.productName = productName;
        this.price = price;
    }

    public MenuItem() {

    }

    public abstract int computePrice(MenuItem item);

    public int getIdItem() {
        return idItem;
    }


    public String getProductName() {
        return productName;
    }


    public int getGrams() {
        return grams;
    }


    public int getPrice() {
        return price;
    }


    public String getDescription() {
        return description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem item = (MenuItem) o;
        return grams == item.grams &&
                price == item.price &&
                Objects.equals(productName, item.productName) &&
                Objects.equals(description, item.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, grams, price, description);
    }
}
