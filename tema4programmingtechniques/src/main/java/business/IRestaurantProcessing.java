package  business;

import java.util.List;
import java.util.Set;

public interface IRestaurantProcessing {
    public abstract void createMenuItem(int id, String ProductName, int Grams, int Price, String Descripion);
    public abstract void deleteMenuItem(int id,String ProductName, int Grams, int Price, String Descripion);
    public abstract void editMenuItem( int id, String ProductName, int Grams, int Price, String Descripion);
    public abstract void createOrder(Order order, Set<MenuItem> menuItem);
    public abstract int computePriceOrder(Order order);
    public abstract void generateBill(List<String> s);

}
