package  business;

import data.FileWriter;

import java.io.Serializable;
import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {
    private Map<Order, Set<MenuItem>> orders;
    private HashSet<MenuItem> menu = new HashSet<MenuItem>();


    public Restaurant() {
        orders = new Hashtable<Order, Set<MenuItem>>();
    }

    public boolean isWellFormed() {
        for (MenuItem m : menu)
            if (m.getPrice() < 0)
                return false;
        return orders != null && menu != null;
    }

    public Map<Order, Set<MenuItem>> getOrders() {
        return orders;
    }

    public void setOrders(Map<Order, Set<MenuItem>> orders) {
        this.orders = orders;
    }

    public HashSet<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(HashSet<MenuItem> menu) {
        this.menu = menu;
    }

    @Override
    public void createMenuItem(int id, String ProductName, int grams, int price, String Descripion) {
        boolean pre = price > 0 && grams > 0 && isWellFormed();
        int size = menu.size();
        assert pre;

        menu.add(new BaseProduct(id, ProductName, grams, price, Descripion));

        boolean post = menu.size() > size && isWellFormed();
        assert post;
    }

    @Override
    public void deleteMenuItem(int id, String ProductName, int Grams, int getPrice, String Descripion) {
        boolean pre =menu.size()>0 && isWellFormed();
        int size = menu.size();
        assert pre;
        menu.remove(new BaseProduct(id, ProductName, Grams, getPrice, Descripion));
    }

    @Override
    public void editMenuItem(int id, String ProductName, int Grams, int getPrice, String Descripion) {
        boolean pre = false;
        for (MenuItem item : menu)
            if (item.getIdItem() == id)
                pre= true;
        pre = pre && getPrice > 0 && Grams > 0 && isWellFormed();
        assert pre;

        int ok = 0;
        MenuItem edit = null;
        for (MenuItem item : menu) {
            if (item.getIdItem() == id) {
                edit = item;
                ok = 1;
            }
        }
        if (ok == 1) {
            if(Descripion.equals("BaseProduct")) {
                menu.remove(edit);
                menu.add(new BaseProduct(id, ProductName, Grams, getPrice, Descripion));
            }else{
                menu.remove(edit);
                menu.add(new CompositeProduct(id, ProductName, Grams, getPrice, Descripion));
            }

        }

        boolean post = false;
        for (MenuItem item : menu)
            if (item.getIdItem() == id)
                post= true;
        post = post && isWellFormed();
        assert post;

    }

    @Override
    public void createOrder(Order order, Set<MenuItem> menuItem) {
        boolean pre = !orders.containsKey(order) && order != null && menuItem != null && isWellFormed();
        assert pre;
        if (orders.containsKey(order)) {
            for (MenuItem item : menuItem)
                orders.get(order).add(item);
        } else {
            orders.put(order, menuItem);
        }

        for (MenuItem item : menuItem) {
            if (item instanceof CompositeProduct) {
                setChanged();
                notifyObservers();
            }
        }

        boolean post = orders.containsKey(order) && order != null && menuItem != null && isWellFormed();
        assert post;
    }

    @Override
    public int computePriceOrder(Order order) {
        boolean pre = order!=null && isWellFormed();
        assert pre;

        int price = 0;
        for (MenuItem item : orders.get(order)) {
            price += item.computePrice(item);
        }

        boolean post = price>0 && isWellFormed();
        assert post;

        return price;
    }

    @Override
    public void generateBill(List<String> s) {
        boolean pre = s!=null && isWellFormed();
        assert pre;

        FileWriter file = new FileWriter();
        file.write(s);
    }

    public int getMaxId() {
        int idMax = 0;
        for (MenuItem item : menu) {
            if (idMax < item.getIdItem())
                idMax = item.getIdItem();
        }
        return idMax;
    }

    public int getMaxOrder() {
        int idMax = 0;
        for (Order order : orders.keySet()) {
            if (idMax < order.getOrderID())
                idMax = order.getOrderID();
        }
        return idMax;
    }
}
