package  business;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CompositeProduct extends MenuItem {

    private  Map<String, Set<MenuItem>>  products = new HashMap<>();
    public CompositeProduct(int id,String productName, int grams, int price, String description) {
        super(id,productName, grams, price, description);
    }

    public CompositeProduct(int id, String productName, int price) {
        super(id, productName, price);
    }

    public CompositeProduct() {
    }

    @Override
    public int computePrice(MenuItem item){
        int price =0;
        for(MenuItem product:products.get(item.getProductName()))
            price+=product.getPrice();
        return  price;
    }

    public void createProuct(Set<MenuItem> product, String productName){
        products.put(productName,product);
    }

    public int computeGrams(Set<MenuItem> prod){
        int grams =0;

        for(MenuItem product:prod){
            System.out.println(product.getGrams());
            grams+=product.getGrams();
        }

        return  grams;
    }
}
